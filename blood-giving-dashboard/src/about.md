#### **Background**

There is a global need for blood giving. Every second of every day, people around the world—of all ages and from all walks of life—need blood transfusions
to survive. The reasons for transfusion vary but the demand for blood is ever-present and growing (blood transfusion is needed not only for a minority of 
people with rare diseases but also to support emergency situations as well as common treatments like surgeries, pregnant women, children with anaemia or 
cancer patients among others). Furthermore, artificial blood is not patient-ready yet (i.e. it can not be produced on an industrial scale for alleviating 
the lack of supply of blood during some periods of time and/or some places) and, if that was not enough, the blood can not be indefinitely stored due to 
the short shelf life of human blood (e.g. red cells up to 35 days or platelets up to seven days).

For all the aforementioned reasons, **every national blood service in the world has to rely on the generosity of blood donors** not only to maintain stock 
levels for all the hospitals, but to provide the necessary range of blood types whenever and wherever they are needed.

#### **Challenge**

One of the most challenging issues regarding the donation of blood is **to maintain a regular supply of blood to all the people who need it during the 
whole year** since predicting the required demand is more than difficult (e.g. blood donations usually drop in the summer months  along with summer vacations
when the necessity of blood normally increases because of the number of accidents and injuries).

#### **Objective**

Adequate and reliable supply of safe blood can only be achieved through a stable network of regular and voluntary blood donors. The aim of this application
is to provide a tool which enables the National/Public agencies all over the world: 

1. to target specific blood types to increase stock levels at any given time (e.g. scheduled surgeries can get delayed or even canceled when blood shortage
is low) through the real-time notification service (push notification).

2. to encourage existing and potential donors to donate blood at regular intervals (e.g. only 4% of adults are currently donors whereas over 25% of us 
require blood at least once in our lifetime) thanks to the geospatial notifications automatically sent to potential donors depending on the location of 
both the donor and the blood bank.

#### **Requirements of the Vaadin/IBM Bluemix challenge**

* Vaadin for the UI: Vaadin version 7.4.1 has been used for the development of the dashboard.
* Available IBM Bluemix services: I have taken advantage of the following services for carrying out the development of the whole service:
	* SQL database for persisting and fetching data from/to a DB.
    * Mobile application security and push service for sending real-time notifications from the dashboard.
* Deployed to IBM Bluemix with public access for final judgement: 
	* [Blood-giving dashboard](http://blood-giving-dashboard.eu-gb.mybluemix.net) (username = admin/password = admin)
* Pick one or more categories below for your app: Map it. 

#### **Sources of information (March, 2015)**

* [World Health Organization](http://www.who.int)
* [National Blood transfusion service, UK](http://www.blood.co.uk)