/** Package */
package com.dashboard.view;

/** Imports */
import org.vaadin.cdiviewmenu.ViewMenuUI;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com)
 * March, 2015
 * 
 * Abstract class which defines the skeleton of the whole application. It includes the content and menu areas.
 *  
 */
@CDIView
public abstract class DashboardView extends MVerticalLayout implements View {


    /** Attributes */
	private static final long serialVersionUID = 1L;
	private CssLayout contentArea = new CssLayout();
    private CssLayout menuArea = new CssLayout();	
	private Button donorsButton;
	private Button bloodBanksButton;	
	//private Button bloodStockButton;	
	private Button aboutButton;	
	private Button signOutButton;
	private MHorizontalLayout menuLayout;	

	/** Methods */	    
    public void setDashboardLayout() {
    	setMargin(true);		
		setSpacing(true);
		setSizeFull();
    }
        
    public void setMenuArea() {    	
    	// Set the buttons
    	donorsButton = new MButton(FontAwesome.USERS,new Button.ClickListener() {              
        	private static final long serialVersionUID = 1L;
        	@Override
        	public void buttonClick(Button.ClickEvent event) {
        		ViewMenuUI.getMenu().navigateTo(DonorListView.class);
        	}
        }).withCaption("List of donors").withStyleName("primary");
    	
    	bloodBanksButton = new MButton(FontAwesome.MAP_MARKER,new Button.ClickListener() {              
        	private static final long serialVersionUID = 1L;
        	@Override
        	public void buttonClick(Button.ClickEvent event) {        		
        		ViewMenuUI.getMenu().navigateTo("bank");        		
        	}
        }).withCaption("Blood banks").withStyleName("primary");
    	
//    	bloodStockButton = new MButton(FontAwesome.BAR_CHART_O,new Button.ClickListener() {              
//        	private static final long serialVersionUID = 1L;
//        	@Override
//        	public void buttonClick(Button.ClickEvent event) {
//        		ViewMenuUI.getMenu().navigateTo(BloodStockView.class);
//        	}
//        }).withCaption("Blood stock levels").withStyleName("primary");
    	
    	aboutButton = new MButton(FontAwesome.INFO,new Button.ClickListener() {              
        	private static final long serialVersionUID = 1L;
        	@Override
        	public void buttonClick(Button.ClickEvent event) {
        		ViewMenuUI.getMenu().navigateTo("about");
        	}
        }).withCaption("About").withStyleName("primary");
    	
    	signOutButton = new MButton(FontAwesome.POWER_OFF,new Button.ClickListener() {              
        	private static final long serialVersionUID = 1L;
        	@Override
        	public void buttonClick(Button.ClickEvent event) {
        		ViewMenuUI.getCurrent().getSession().close();
        		ViewMenuUI.getMenu().navigateTo(LoginView.class);
        	}
        }).withCaption("Sign out").withStyleName("primary");
    	 
        // Set the layout
		menuLayout = new MHorizontalLayout(donorsButton,bloodBanksButton,aboutButton,signOutButton).
				expand(donorsButton,bloodBanksButton,aboutButton,signOutButton).withFullWidth().withMargin(false).withSpacing(false);
		menuArea.addComponent(menuLayout);
		menuArea.setStyleName("menu-area");
		menuArea.setWidth("100%");
		addComponents(contentArea,menuArea);          	 
    }
    
    abstract void setContentArea();    
    
    public void manageMenuButton(String view,boolean value) {
    	switch (view) {
        case "donorListView":
        	if (value)
        		donorsButton.setEnabled(true);
        	else
        		donorsButton.setEnabled(false);
            break;
        case "bloodBankView":
        	if (value)
        		bloodBanksButton.setEnabled(true);
        	else
        		bloodBanksButton.setEnabled(false);
            break;        	
//        case "bloodStockView":
//        	if (value)
//        		bloodStockButton.setEnabled(true);
//        	else
//        		bloodStockButton.setEnabled(false);              
//            break;
        case "aboutView":
        	if (value)
        		aboutButton.setEnabled(true);
        	else
        		aboutButton.setEnabled(false);        	
            break;        
        case "signOutView":
        	if (value)
        		signOutButton.setEnabled(true);
        	else
        		signOutButton.setEnabled(false);        	
            break;
        default:
            throw new IllegalArgumentException("[ERROR] Invalid name of the view: " + view);
    	}
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    	
    }
	
}