package com.dashboard.view;

import javax.annotation.PostConstruct;

import org.vaadin.viritin.label.Header;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;

@CDIView("stock")
public class BloodStockView extends MVerticalLayout implements View {


    @PostConstruct
    void init() {

        add(new Header("Menu").setHeaderLevel(2));        
        setMargin(new MarginInfo(false, true, true, true));
        setStyleName("menu-view");
        
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        
    }
}
