/** Package */
package com.dashboard.view;

/** Imports */

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com)
 * March, 2015
 * 
 * Constants
 *  
 */
public class Constants {
	
	public static final String LOGIN_VIEW = "loginView";
	public static final String DONOR_LIST_VIEW = "donorListView";
	public static final String BLOOD_BANK_VIEW = "bloodBankView";
	public static final String ABOUT_VIEW = "aboutView";
	
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	
	//FIXME fix this
	public static int notificationReceivers = 29;

}