/** Package */
package com.dashboard.view;

/** Imports */
import javax.annotation.PostConstruct;

import org.vaadin.viritin.label.Header;
import org.vaadin.viritin.label.RichText;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import com.dashboard.presenter.ScreenSize;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * About view. A piece of text about the aim of the project
 *  
 */
@CDIView("about")
public class AboutView extends DashboardView {


    /** Attributes */
	private static final long serialVersionUID = 1L;		
		
	VerticalLayout content = new VerticalLayout();
	Panel panel = new Panel();
	Header header = new Header("About").setHeaderLevel(2);  
	RichText text = new RichText().withMarkDownResource("/about.md");
	Link followButton = new Link("Follow me",new ExternalResource("https://twitter.com/angel_cavero"));
	Link forkButton = new Link("Fork me",new ExternalResource("https://bitbucket.org/anmarca6/blood-giving-dashboard")); 	
	
	/** Methods */
	@PostConstruct
	public void init() {		   	
		// Set up layout
		setDashboardLayout();		
		setContentArea();		
		setMenuArea();   		
		// Disable button
		manageMenuButton(Constants.ABOUT_VIEW,false);
	}
	
	@Override
	void setContentArea() {
		// Links		 	   
		followButton.addStyleName("large");
		followButton.addStyleName("link-button");
		followButton.setIcon(FontAwesome.TWITTER);		   
		forkButton.addStyleName("large");
		forkButton.addStyleName("link-button");
		forkButton.setIcon(FontAwesome.BITBUCKET);
//		linksLayout = new HorizontalLayout();
//		linksLayout.setSpacing(true);
//		linksLayout.setMargin(true);
//		linksLayout.addStyleName("links-panel");
//		linksLayout.addComponents(followButton,forkButton);        
        // Responsive layout
        layout();        
        UI.getCurrent().setResizeLazy(true);
        Page.getCurrent().addBrowserWindowResizeListener(new Page.BrowserWindowResizeListener() {                    
        	private static final long serialVersionUID = 1L;
        	@Override
        	public void browserWindowResized(Page.BrowserWindowResizeEvent browserWindowResizeEvent) {        		
        		layout();
        	}
        });        	
	}
	
	private void layout() {
        removeAllComponents();        
        if (ScreenSize.getScreenSize() == ScreenSize.LARGE) {
        	content.addComponent(text); 
        	
        } else {
        	addComponents(new MHorizontalLayout(header).alignAll(Alignment.BOTTOM_LEFT));
        	add(new RichText().withMarkDownResource("/about.md"));
        	//addComponents(new MHorizontalLayout(followButton).alignAll(Alignment.BOTTOM_LEFT));
        }        
        content.setWidth("100%");        
        panel.setSizeFull();        
        //setMargin(new MarginInfo(false,true,true,true));
        //expand(text);        
        panel.setContent(content);
        setMargin(new MarginInfo(false,true,false,true));
        //addComponents(header,panel);
        
        addComponents(new MHorizontalLayout(header,followButton,forkButton).expand(header).alignAll(Alignment.MIDDLE_LEFT),panel);
        
        expand(panel);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        
    }
    
}