/** Package */
package com.dashboard.view;

/** Imports */
import java.util.Date;
import org.vaadin.viritin.button.MButton;
import com.dashboard.model.Donor;
import com.dashboard.model.DonorBloodType;
import com.dashboard.model.DonorGender;
import com.dashboard.model.DonorLocation;
import com.dashboard.model.DonorRhFactor;
import com.dashboard.model.DonorService;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.UserError;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * Donor form
 * 
 * Licensed under the GNU Affero General Public License, Version 3.0 (AGPL-3.0); you may not use this file except in compliance with the License. 
 * You may obtain a copy of the license at:
 * 
 * https://www.gnu.org/licenses/agpl-3.0.html 
 *   
 */
@CDIView
public class DonorForm extends VerticalLayout implements View {

	/** Attributes */	
	private static final long serialVersionUID = 1L;
	private TextField name;
	private TextField lastName;
	private OptionGroup gender;
	private OptionGroup bloodType;
	private OptionGroup rhFactor;
	private OptionGroup location;
	private DateField lastDonation;
	private TextField email;
	private DonorService service;

	/** Constructor */

	//FIXME To learn how to inject beans
	public DonorForm(DonorService s) {
		service = s;
	}

	
	public void init() {
		setSpacing(true);
		setMargin(true); 		

		final FormLayout form = new FormLayout();
		form.setImmediate(true);
		form.setMargin(true);
		form.setSpacing(true);
		form.setWidth("700px");      
		form.addStyleName("light");        

		Label section = new Label("Personal information");
		section.addStyleName("h4");
		section.addStyleName("colored");
		form.addComponent(section);        

		name = new TextField("First name");       
		name.setWidth("75%");
		name.setRequired(true);
		form.addComponent(name);

		lastName = new TextField("Last name");       
		lastName.setWidth("75%");
		lastName.setRequired(true);
		form.addComponent(lastName);               

		gender = new OptionGroup("Gender");
		gender.setRequired(true);
		gender.addItem(0);
		gender.setItemCaption(0,"Female");
		gender.addItem(1);
		gender.setItemCaption(1,"Male");       
		gender.addStyleName("horizontal");        
		form.addComponent(gender);

		bloodType = new OptionGroup("Blood type");
		bloodType.setRequired(true);
		bloodType.addItem(0);
		bloodType.setItemCaption(0,"0");
		bloodType.addItem(1);
		bloodType.setItemCaption(1,"A");
		bloodType.addItem(2);
		bloodType.setItemCaption(2,"B");
		bloodType.addItem(3);
		bloodType.setItemCaption(3,"AB");                        
		bloodType.addStyleName("horizontal");
		form.addComponent(bloodType);

		rhFactor = new OptionGroup("Rh factor");
		rhFactor.setRequired(true);
		rhFactor.addItem(0);
		rhFactor.setItemCaption(0,"Positive");
		rhFactor.addItem(1);
		rhFactor.setItemCaption(1,"Negative");                           
		rhFactor.addStyleName("horizontal");
		form.addComponent(rhFactor);

		location = new OptionGroup("Location");
		location.setRequired(true);
		location.addItem(0);
		location.setItemCaption(0,"Castellon");
		location.addItem(1);
		location.setItemCaption(1,"Valencia");
		location.addItem(2);
		location.setItemCaption(2,"Alicante");              
		location.addStyleName("horizontal");
		form.addComponent(location);               

		lastDonation = new DateField("Last donation"); 
		lastDonation.setWidth("75%");
		form.addComponent(lastDonation); 

		section = new Label("Contact information");
		section.addStyleName("h4");
		section.addStyleName("colored");
		form.addComponent(section);

		email = new TextField("Email");        
		email.setWidth("75%");
		email.setRequired(false);
		form.addComponent(email);        

		VerticalLayout wrap = new VerticalLayout();
		wrap.setSpacing(true);
		wrap.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
		wrap.setCaption("Notification");
		CheckBox pushNotification = new CheckBox("Send me a notification whenever my blood type is required",true);        
		CheckBox geoNotification = new CheckBox("Send me a notification when a blood bank is near to me",true);        
		wrap.addComponent(pushNotification);
		wrap.addComponent(geoNotification);        
		form.addComponent(wrap);          

		Button saveButton = new MButton("Save",new Button.ClickListener() {              
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(Button.ClickEvent event) {       		
				// Check status required fields
				if (isReady()) {
					Donor c = new Donor();            		
					c.setFirstName(name.getValue().toString());
					c.setLastName(lastName.getValue().toString());

					if (email.getValue().toString().isEmpty()) 
						c.setEmail(name.getValue().toLowerCase() + "@" + lastName.getValue().toLowerCase() + ".com");       
					else
						c.setEmail(email.getValue().toLowerCase());

					String genderValueString = String.valueOf(gender.getValue());
					if (genderValueString.equals("0"))   
						c.setGender(DonorGender.Female);            		           		
					else
						c.setGender(DonorGender.Male);

					String locationValueString = String.valueOf(location.getValue());					
					if (locationValueString.equals("0"))   
						c.setLocation(DonorLocation.Castellon);            		           		
					else if (locationValueString.equals("1"))
						c.setLocation(DonorLocation.Valencia);
					else
						c.setLocation(DonorLocation.Alicante);

					String bloodValueString = String.valueOf(bloodType.getValue());					
					if (bloodValueString.equals("0"))   
						c.setBloodType(DonorBloodType.O);            		           		
					else if (locationValueString.equals("1"))
						c.setBloodType(DonorBloodType.A);
					else if (locationValueString.equals("2"))
						c.setBloodType(DonorBloodType.B);
					else
						c.setBloodType(DonorBloodType.AB);

					
					String factorValueString = String.valueOf(rhFactor.getValue());
					if (factorValueString.equals("0"))   
						c.setRhFactor(DonorRhFactor.Positive);            		           		
					else
						c.setRhFactor(DonorRhFactor.Negative);
				
					c.setTotalDonations(1);        		
					c.setBirthDate(new Date());					
					c.setLastDonation(new Date());					
					c.setDonorStatus("Allow");
					c.setContacted("Yes");					
					service.saveEntity(c);
					Notification.show("New donor properly saved",Notification.Type.WARNING_MESSAGE);
					resetFields();
				} else {
					Notification.show("Please review the form. Some parameters are still missing",Notification.Type.WARNING_MESSAGE);
				}
			}
		});        
		saveButton.addStyleName("primary");
		addComponents(form,saveButton);
		setComponentAlignment(saveButton,Alignment.MIDDLE_CENTER);
	}    

    @Override
    public void enter(ViewChangeEvent event) {        

    }   

    public boolean isReady() {
    	name.setComponentError(null);    	
    	lastName.setComponentError(null);
    	gender.setComponentError(null);
    	bloodType.setComponentError(null);
    	rhFactor.setComponentError(null);
    	location.setComponentError(null);
    	boolean isValid = true;
    	if(name.getValue().toString().isEmpty()) {
    		name.setComponentError(new UserError("First name is required"));
    		isValid = false;
    	}
    	if(lastName.getValue().toString().isEmpty()) {
    		lastName.setComponentError(new UserError("Last name is required"));
    		isValid = false;
    	}
    	if(gender.getValue() == null) {
    		gender.setComponentError(new UserError("Gender is required"));
    		isValid = false;
    	}
    	if(bloodType.getValue() == null) {
    		bloodType.setComponentError(new UserError("Blood type is required"));
    		isValid = false;
    	}
    	if(rhFactor.getValue() == null) {
    		rhFactor.setComponentError(new UserError("Rh factor is required"));
    		isValid = false;
    	}
    	if(location.getValue() == null) {
    		location.setComponentError(new UserError("Location is required"));
    		isValid = false;
    	}    	
    	return isValid;
    }
    
    public void resetFields() {
    	name.setValue("");
		lastName.setValue("");
		email.setValue("");
		lastDonation.setValue(null);
		gender.setValue(null);
		bloodType.setValue(null);
		rhFactor.setValue(null);
		location.setValue(null);
    }

}