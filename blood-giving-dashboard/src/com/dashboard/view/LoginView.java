/** Package */
package com.dashboard.view;

/** Imports */
import org.vaadin.cdiviewmenu.ViewMenuUI;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import com.vaadin.cdi.CDIView;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.shared.Position;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * Login view class. Landing page for accessing the the dashboard (valid username and password are required)
 *  
 */
@CDIView("")
public class LoginView extends VerticalLayout implements View {
	

	/** Attributes */
	private static final long serialVersionUID = 1L;
	private VerticalLayout backgroundImage;
	private CssLayout loginPanel;
	private TextField username;
	private PasswordField password;
	private Button signin;
	private static final String USERNAME = "admin";
	private static final String PASSWORD = "admin";
	private HorizontalLayout linksLayout;	
	private Link photoCreditsButton;
	
	/** Methods */

	@Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {		
		removeAllComponents();		
		setSizeFull();
		setBackgroundImage();
		buildLoginPanel();        
    }
	
	private void setBackgroundImage() {
		// Set background image
		backgroundImage = new VerticalLayout();
		backgroundImage.setWidth(Page.getCurrent().getBrowserWindowWidth(),Unit.PIXELS);
		backgroundImage.setHeight(Page.getCurrent().getBrowserWindowHeight(),Unit.PIXELS);
		backgroundImage.addStyleName("background-image");		
		addComponent(backgroundImage);
	}
	
	private void buildLoginPanel() {
		// Build panel
		loginPanel = new CssLayout();
        loginPanel.addStyleName("login-panel");        
        // Build labels
        HorizontalLayout labels = new HorizontalLayout();
        labels.setWidth("100%");
        labels.setHeight("70px");
        labels.setMargin(true);
        labels.addStyleName("labels");        		
        Label welcome = new Label("Welcome");
        welcome.setSizeUndefined();
        welcome.addStyleName("huge");
        welcome.addStyleName("colored");     
        labels.addComponent(welcome);
        labels.setComponentAlignment(welcome,Alignment.TOP_LEFT);
        // Build fields
        HorizontalLayout fields = new MHorizontalLayout();
        fields.setSpacing(true);
        fields.setMargin(true);
        fields.addStyleName("fields");
        username = new TextField("Username");
        username.setInputPrompt("Username");
        username.setIcon(FontAwesome.USER);
        username.addStyleName("large");
        username.addStyleName("inline-icon"); 
        username.setRequired(true);
        password = new PasswordField("Password");
        password.setInputPrompt("Password");
        password.setIcon(FontAwesome.LOCK);
        password.addStyleName("large");
        password.addStyleName("inline-icon"); 
        password.setRequired(true);
        signin = new Button("Sign in");
        signin.addStyleName("primary");
        signin.setClickShortcut(KeyCode.ENTER);
        signin.focus();        
        fields.addComponents(new MHorizontalLayout(username,password,signin).alignAll(Alignment.BOTTOM_LEFT).expand(signin));        
        signin.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
            public void buttonClick(ClickEvent event) {				
				if (isInputFormValid()) {					
					// Check username and password					
					if(username.getValue().equals(USERNAME) && password.getValue().equals(PASSWORD)) {						
						successfulLogin();
					} else {						
						failureLogin();
					}						
				}
            }
        });
       
       // Build link button       
       photoCreditsButton = new Link("Image courtesy of P. Chevrolet",new ExternalResource("http://goo.gl/yDF7Z8")); 	   
       photoCreditsButton.addStyleName("large");
       photoCreditsButton.addStyleName("link-button");
       photoCreditsButton.setIcon(FontAwesome.FLICKR);
       
       linksLayout = new HorizontalLayout();
       linksLayout.setSpacing(true);
       linksLayout.setMargin(true);
       linksLayout.addStyleName("links-panel");
       linksLayout.addComponents(photoCreditsButton);
       
		// Add components
        loginPanel.addComponent(labels);
        loginPanel.addComponent(fields);
        addComponent(loginPanel);        
        addComponent(linksLayout);
    }
	
	private boolean isInputFormValid() {
		// Reset prior errors
		username.setComponentError(null);
		password.setComponentError(null);
		boolean isValid = true;
		// If the user does not type any value, an error message is shown
		if (username.getValue().isEmpty() && password.getValue().isEmpty()) {	        
	        username.setComponentError(new UserError("Username is required"));
	        password.setComponentError(new UserError("Password is required"));
			isValid = false;		
		} else if (username.getValue().isEmpty()) {
			username.setComponentError(new UserError("Username is required"));	        
			isValid = false;
		} else if (password.getValue().isEmpty()) {			
	        password.setComponentError(new UserError("Password is required"));
			isValid = false;
		}
		return isValid;
	} 
		
	private void successfulLogin() {		
		// Navigate to the list of donors				
        ViewMenuUI.getMenu().navigateTo(DonorListView.class);        
	}

	private void failureLogin() {
		// Show error
		Notification fail = new Notification("Invalid username or password");
		fail.setDelayMsec(1500);
		fail.setPosition(Position.MIDDLE_CENTER);
		// Init state
        resetAll();
	}
	
	private void resetAll() {
		// Errors
		username.setComponentError(null);
		password.setComponentError(null);
		// Text fields
		username.setValue("");
		username.setInputPrompt("Username");
		password.setValue("");
		password.setInputPrompt("Password");
		// Focus
		username.focus();
	}

}