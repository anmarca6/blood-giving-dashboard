/** Package */
package com.dashboard.view;

/** Imports */
import org.vaadin.viritin.button.MButton;
import com.dashboard.model.BloodBank;
import com.dashboard.model.BloodBankService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.UserError;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * Blood bank form
 * 
 * Licensed under the GNU Affero General Public License, Version 3.0 (AGPL-3.0); you may not use this file except in compliance with the License. 
 * You may obtain a copy of the license at:
 * 
 * https://www.gnu.org/licenses/agpl-3.0.html 
 *   
 */
public class BloodBankForm extends VerticalLayout implements View {
    
	/** Attributes */	
	private static final long serialVersionUID = 1L;
	private TextField town;	
	private OptionGroup mobility;
	private OptionGroup timetableFrom;
	private OptionGroup timetableTo;
	private OptionGroup city;	
	private DateField availability;
	private TextField latitude;
	private TextField longitude;
	private BloodBankService service;

	/** Constructor */

	//FIXME To learn how to inject beans
	public BloodBankForm(BloodBankService s) {
		service = s;
	}

	public void init() {
		
        setSpacing(true);
        setMargin(true); 		

        final FormLayout form = new FormLayout();
        form.setImmediate(true);
        form.setMargin(true);
        form.setSpacing(true);
        form.setWidth("700px");      
        form.addStyleName("light");        

        Label section = new Label("Blood bank information");
        section.addStyleName("h4");
        section.addStyleName("colored");
        form.addComponent(section);   
        
        mobility = new OptionGroup("Mobility");
        mobility.setRequired(true);
        mobility.addItem(0);
        mobility.setItemCaption(0,"Permanent");
        mobility.addItem(1);
        mobility.setItemCaption(1,"Mobile");       
        mobility.addStyleName("horizontal");        
        form.addComponent(mobility);
        
        city = new OptionGroup("City");
        city.setRequired(true);
        city.addItem(0);
        city.setItemCaption(0,"Castellon");
        city.addItem(1);
        city.setItemCaption(1,"Valencia");
        city.addItem(2);
        city.setItemCaption(2,"Alicante");              
        city.addStyleName("horizontal");
        form.addComponent(city);               
        
        town = new TextField("Town");       
        town.setWidth("75%");
        town.setRequired(true);
        form.addComponent(town);
        
        availability = new DateField("Date"); 
        availability.setWidth("75%");
        availability.setRequired(true);
        form.addComponent(availability); 
        
        timetableFrom = new OptionGroup("Timetable (from)");
        timetableFrom.setRequired(true);
        timetableFrom.addItem(0);
        timetableFrom.setItemCaption(0,"08:00 am");
        timetableFrom.addItem(1);
        timetableFrom.setItemCaption(1,"08:30 am");
        timetableFrom.addItem(2);
        timetableFrom.setItemCaption(2,"09:00 am");
        timetableFrom.addItem(3);
        timetableFrom.setItemCaption(3,"09:30 am");                        
        timetableFrom.addStyleName("horizontal");
        form.addComponent(timetableFrom); 

        timetableTo = new OptionGroup("Timetable (to)");
        timetableTo.setRequired(true);
        timetableTo.addItem(0);
        timetableTo.setItemCaption(0,"10:00 am");
        timetableTo.addItem(1);
        timetableTo.setItemCaption(1,"10:30 am");
        timetableTo.addItem(2);
        timetableTo.setItemCaption(2,"11:00 am");
        timetableTo.addItem(3);
        timetableTo.setItemCaption(3,"11:30 am");                        
        timetableTo.addStyleName("horizontal");
        form.addComponent(timetableTo);  
        
        latitude = new TextField("Latitude");       
        latitude.setWidth("75%");
        latitude.setRequired(true);
        form.addComponent(latitude);
        
        longitude = new TextField("Longitude");       
        longitude.setWidth("75%");
        longitude.setRequired(true);
        form.addComponent(longitude);
        
        Button saveButton = new MButton("Save",new Button.ClickListener() {              
        	private static final long serialVersionUID = 1L;
        	@Override
        	public void buttonClick(Button.ClickEvent event) {       		
        		// Check status required fields
        		if (isReady()) {
        			BloodBank b = new BloodBank();  
        			
        			String mobilityValueString = String.valueOf(mobility.getValue());
            		if (mobilityValueString.equals("0"))   
            			b.setMobility("Permanent");            		           		
            	    else
            	    	b.setMobility("Mobile");
        			
        			b.setAvailability(availability.getValue());
        			
        			String fromValueString = String.valueOf(timetableFrom.getValue());
            		if (fromValueString.equals("0"))   
            			b.setTimetableFrom("08:00 am");            		           		
            		else if (fromValueString.equals("1"))
            			b.setTimetableFrom("08:30 am");
            		else if (fromValueString.equals("2"))
            			b.setTimetableFrom("09:00 am");
            		else
            			b.setTimetableFrom("09:30 am");
            		
            		String toValueString = String.valueOf(timetableTo.getValue());
            		if (toValueString.equals("0"))   
            			b.setTimetableTo("10:00 am");            		           		
            		else if (toValueString.equals("1"))
            			b.setTimetableTo("10:30 am");
            		else if (toValueString.equals("2"))
            			b.setTimetableTo("11:00 am");
            		else
            			b.setTimetableTo("11:30 am");
        			
        			String cityValueString = String.valueOf(city.getValue());
            		if (cityValueString.equals("0"))   
            			b.setCity("Castellon");            		           		
            		else if (cityValueString.equals("1"))
            			b.setCity("Valencia");
            		else
            			b.setCity("Alicante");
        			
        			b.setTown(town.getValue().toString());
        			GeometryFactory factory = new GeometryFactory();
        			double lat = Double.parseDouble(latitude.getValue().toString());
        			double lon = Double.parseDouble(longitude.getValue().toString());
        			b.setLocation(factory.createPoint(new Coordinate(lon,lat)));      
        			service.saveEntity(b);
        			Notification.show("New blood bank properly saved",Notification.Type.WARNING_MESSAGE);
					resetFields();
				} else {
					Notification.show("Please review the form. Some parameters are still missing",Notification.Type.WARNING_MESSAGE);
				} 
        	}
        });        
        saveButton.addStyleName("primary");        
        addComponents(form,saveButton);
        setComponentAlignment(saveButton,Alignment.MIDDLE_CENTER);        
    }

    @Override
    public void enter(ViewChangeEvent event) {        

    }   

    public boolean isReady() {
    	town.setComponentError(null);    	
    	mobility.setComponentError(null);
    	timetableFrom.setComponentError(null);
    	timetableTo.setComponentError(null);
    	city.setComponentError(null);
    	availability.setComponentError(null);
    	latitude.setComponentError(null);
    	longitude.setComponentError(null);    	
    	boolean isValid = true;
    	if(town.getValue().toString().isEmpty()) {
    		town.setComponentError(new UserError("Town is required"));
    		isValid = false;
    	}
    	if(latitude.getValue().toString().isEmpty()) {
    		latitude.setComponentError(new UserError("Latitude is required"));
    		isValid = false;
    	}
    	if(longitude.getValue().toString().isEmpty()) {
    		longitude.setComponentError(new UserError("Longitude is required"));
    		isValid = false;
    	}
    	if(mobility.getValue() == null) {
    		mobility.setComponentError(new UserError("Mobility is required"));
    		isValid = false;
    	}
    	if(timetableFrom.getValue() == null) {
    		timetableFrom.setComponentError(new UserError("Timetable (from) is required"));
    		isValid = false;
    	}
    	if(timetableTo.getValue() == null) {
    		timetableTo.setComponentError(new UserError("Timetable (to) is required"));
    		isValid = false;
    	}
    	if(city.getValue() == null) {
    		city.setComponentError(new UserError("City is required"));
    		isValid = false;
    	}
    	if(availability.getValue() == null) {
    		availability.setComponentError(new UserError("Date is required"));
    		isValid = false;
    	}    	
    	return isValid;
    }
    
    public void resetFields() {
    	town.setValue("");
		longitude.setValue("");
		latitude.setValue("");
		availability.setValue(null);
		mobility.setValue(null);
		city.setValue(null);
		timetableFrom.setValue(null);
		timetableTo.setValue(null);
    }

}