/** Package */
package com.dashboard.view;

/** Imports */
import org.vaadin.viritin.button.MButton;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.UserError;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * Notification form
 * 
 * Licensed under the GNU Affero General Public License, Version 3.0 (AGPL-3.0); you may not use this file except in compliance with the License. 
 * You may obtain a copy of the license at:
 * 
 * https://www.gnu.org/licenses/agpl-3.0.html 
 *   
 */
@CDIView
public class NotificationForm extends VerticalLayout implements View {

	/** Attributes */	
	private static final long serialVersionUID = 1L;
	private Label label;
	private TextArea message;
	private int receivers;
	

	/** Constructor */	
	public NotificationForm(int r) {
		receivers = r;
	}

	
	public void init() {
		setSpacing(true);
		setMargin(true); 		

		final FormLayout form = new FormLayout();
		form.setImmediate(true);
		form.setMargin(true);
		form.setSpacing(true);
		form.setWidth("700px");      
		form.addStyleName("light");        

		Label section = new Label("Notification");
		section.addStyleName("h4");
		section.addStyleName("colored");
		form.addComponent(section);        

		label = new Label("A notification will be sent to " + receivers + " receivers");
        label.addStyleName("large");
        form.addComponent(label); 

        message = new TextArea("Message");
        message.addStyleName("large");
        message.setInputPrompt("Each blood donation can help as many as 3 people. Thank you very much for helping us to save lives");
        message.setRequired(true);
        form.addComponent(message);        
        
		Button sendNotification = new MButton("Send",new Button.ClickListener() {              
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(Button.ClickEvent event) {       		
				// Check status required fields
				if (isReady()) {					
					Notification.show("A notification has been properly sent",Notification.Type.WARNING_MESSAGE);
					resetFields();
				} else {
					Notification.show("Please review the form. Some parameters are still missing",Notification.Type.WARNING_MESSAGE);
				}
			}
		});        
		sendNotification.addStyleName("primary");
		addComponents(form,sendNotification);
		setComponentAlignment(sendNotification,Alignment.MIDDLE_CENTER);
	}    

    @Override
    public void enter(ViewChangeEvent event) {        

    }   

    public boolean isReady() {
    	message.setComponentError(null);    	
    	boolean isValid = true;
    	if(message.getValue().toString().isEmpty()) {
    		message.setComponentError(new UserError("Message is required"));
    		isValid = false;
    	}    	    	
    	return isValid;
    }
    
    public void resetFields() {
    	message.setValue("");		
    }

}