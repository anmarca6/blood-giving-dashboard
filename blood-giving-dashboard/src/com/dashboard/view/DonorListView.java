/** Package */
package com.dashboard.view;

/** Imports */
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.vaadin.cdiviewmenu.ViewMenuUI;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.fields.MTable;
import org.vaadin.viritin.fields.MValueChangeEvent;
import org.vaadin.viritin.fields.MValueChangeListener;
import org.vaadin.viritin.label.Header;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import com.dashboard.model.Donor;
import com.dashboard.model.DonorBloodType;
import com.dashboard.model.DonorLocation;
import com.dashboard.model.DonorService;
import com.dashboard.presenter.ScreenSize;
import com.dashboard.view.DonorEvent.Type;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * A view that lists all the donors available in the DB in a table and lets the user to choose one for edition
 *  
 */
@CDIView("donors")
public class DonorListView extends DashboardView {

    /** Attributes */
	private static final long serialVersionUID = 1L;
	private static final String VIEW_NAME = "donorListView";

	@Inject
    private DonorService service;

//    @Inject
//    DonorForm_ form;

    // GUI    
	@SuppressWarnings({ "unchecked", "rawtypes" })	
	MTable<Donor> donorTable = new MTable(Donor.class).withFullWidth().withFullHeight();	
    MHorizontalLayout mainContent = new MHorizontalLayout(donorTable).withFullWidth().withMargin(false);
    Header header = new Header("List of donors").setHeaderLevel(2);   
    OptionGroup options = new OptionGroup();
    TextField filterByName = new TextField();
    TextField filterByBloodType = new TextField();
    TextField filterByLocation = new TextField();    
    Button addButton = new MButton(FontAwesome.EDIT,new Button.ClickListener() {              
    	private static final long serialVersionUID = 1L;
    	@Override
    	public void buttonClick(Button.ClickEvent event) {
    		addDonor();
    	}
    });  
    Button notificationButton = new MButton(FontAwesome.BULLHORN,new Button.ClickListener() {              
    	private static final long serialVersionUID = 1L;
    	@Override
    	public void buttonClick(Button.ClickEvent event) {
    		sendNotification();
    	}
    });    
    
    /** Methods */
    @PostConstruct
    public void init() {
    	// Generate some dummy data
    	service.resetRandomData();    	
    	// Set up layout
    	setDashboardLayout();
    	setContentArea();
    	setMenuArea();    	      
    	// Disable button
    	manageMenuButton(VIEW_NAME,false);
    }

    // To optimize the layout depending on the screen size
    private void layout() {
        removeAllComponents();
        if (ScreenSize.getScreenSize() == ScreenSize.LARGE) {
        	addComponents(new MHorizontalLayout(header).alignAll(Alignment.BOTTOM_LEFT));            
        	addComponents(new MHorizontalLayout(options,filterByName,filterByBloodType,filterByLocation,addButton,notificationButton).expand(notificationButton).alignAll(Alignment.MIDDLE_LEFT),mainContent);       	
        } else {
            addComponents(header,new MHorizontalLayout(filterByBloodType,addButton).expand(filterByBloodType).alignAll(Alignment.MIDDLE_LEFT),mainContent);
        }
        setMargin(new MarginInfo(false,true,false,true));
        expand(mainContent);
    }

    // Similarly to layouts, we can adapt component configurations based on the client details
    private void adjustTableColumns() {    	
    	// One (generated) column with combined first + last name    	
        if (donorTable.getColumnGenerator("donor-status") == null) {
        	donorTable.addGeneratedColumn("donor-status",new Table.ColumnGenerator() {                           
        		private static final long serialVersionUID = 1L;
        		@Override
        		public Object generateCell(Table table,Object o,Object o2) {
        			Donor c = (Donor) o;
        			if (c.getDonorStatus().equals("Allow")) {
        				Resource res = new ThemeResource("img/green.png");
        				return new Image(null,res);
        			} else {
        				Resource res = new ThemeResource("img/red.png");
        				return new Image(null,res);
        			}        			
        		}
        	});
        }
    	
    	// One (generated) column with combined first + last name    	
        if (donorTable.getColumnGenerator("name") == null) {
        	donorTable.addGeneratedColumn("name",new Table.ColumnGenerator() {                           
        		private static final long serialVersionUID = 1L;
        		@Override
        		public Object generateCell(Table table,Object o,Object o2) {
        			Donor c = (Donor) o;
        			return c.getFirstName() + " " + c.getLastName();
        		}
        	});
        }      
        
        // One (generated) column with combined blood type + rh factor
        if (donorTable.getColumnGenerator("blood") == null) {
        	donorTable.addGeneratedColumn("blood",new Table.ColumnGenerator() {                           
        		private static final long serialVersionUID = 1L;
        		public Object generateCell(Table table,Object o,Object o2) {
        			Donor c = (Donor) o;
        			return c.getBloodType() + " " + c.getRhFactor();
        		}
        	});
        } 
        
        // Center table content        
        donorTable.setColumnAlignment("donor-status",Table.Align.CENTER);
        donorTable.setColumnAlignment("name",Table.Align.CENTER);
        donorTable.setColumnAlignment("blood",Table.Align.CENTER);
        donorTable.setColumnAlignment("location",Table.Align.CENTER);
        
        
        // Show more or less number of columns depending on the size of the screen
        if (ScreenSize.getScreenSize() == ScreenSize.LARGE) {        
        	donorTable.setVisibleColumns("donor-status","name","blood","location");
        	donorTable.setColumnHeaders("Status","Name","Blood type","Location");        	 
        } else if (ScreenSize.getScreenSize() == ScreenSize.MEDIUM) {
        	donorTable.setVisibleColumns("name","blood");
        	donorTable.setColumnHeaders("Name","Blood type");
        } else {
        	donorTable.setVisibleColumns("name");
        	donorTable.setColumnHeaders("Name");
        }        
        
    }

    private void listDonors() {
        donorTable.setBeans(new ArrayList<>(service.findAll()));
    }    
    
    private void listDonors(String filterString) {
        donorTable.setBeans(new ArrayList<>(service.findByName(filterString)));
    }
    
    private void listDonorsByBloodType(DonorBloodType filter) {
        donorTable.setBeans(new ArrayList<>(service.findByBloodType(filter)));
    }
    
    private void listDonorsByLocation(DonorLocation filter) {
        donorTable.setBeans(new ArrayList<>(service.findByLocation(filter)));
    }
    
    private void listDonorsByStatus(String filterString) {
        donorTable.setBeans(new ArrayList<>(service.findByStatus(filterString)));
    }

    void editDonor(Donor customer) {
//        if (customer != null) {
//            openEditor(customer);
//        } else {
//            closeEditor();
//        }   
    }

    void addDonor() {
        //openEditor(new Donor());
    	final Window window = new Window("Add new donor");
    	window.setModal(true);
    	window.setClosable(true);
    	window.setResizable(false);
    	window.setSizeUndefined();
        window.center();
        final DonorForm content = new DonorForm(service);
        content.init();
        window.setContent(content); 
        ViewMenuUI.getCurrent().addWindow(window);
    }
    
    public void sendNotification() {        
    	final Window window = new Window("Send notification");
    	window.setModal(true);
    	window.setClosable(true);
    	window.setResizable(false);
    	window.setSizeUndefined();
        window.center();
        final NotificationForm content = new NotificationForm(Constants.notificationReceivers);
        content.init();
        window.setContent(content); 
        ViewMenuUI.getCurrent().addWindow(window);
    }

//    private void openEditor(Donor donor) {
//        form.setEntity(donor);
//        System.out.println("uno");
//        if (ScreenSize.getScreenSize() == ScreenSize.LARGE) {
//        	System.out.println("dos");
//            mainContent.addComponent(form);
//            System.out.println("tres");
//            form.focusFirst();
//        } else {            
//            MainUI.get().getContentLayout().replaceComponent(this,form);
//            
//        }
//    }

//    private void closeEditor() {        
//        if (form.getParent() == mainContent) {
//            mainContent.removeComponent(form);
//        } else {
//            MainUI.get().getContentLayout().replaceComponent(form,this);
//        }
//    }

    // Methods called by the CDI event system
    void saveCustomer(@Observes @DonorEvent(Type.SAVE) Donor customer) {
        listDonors();
        //closeEditor();
    }

    void resetCustomer(@Observes @DonorEvent(Type.REFRESH) Donor customer) {
        listDonors();
        //closeEditor();
    }

    void deleteCustomer(@Observes @DonorEvent(Type.DELETE) Donor customer) {
        //closeEditor();
        listDonors();
    } 

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {    	
    	
    }

	@Override
	public void setContentArea() {		
		//
		donorTable.setSelectable(false);		
		// Add description
    	addButton.setDescription("Add new donor");
    	// Style
    	notificationButton.addStyleName("icon-align-right");
    	notificationButton.setCaption("Send notification");
        // Value change listener which opens the selected customer into the form
        donorTable.addMValueChangeListener(new MValueChangeListener<Donor>() {            
			private static final long serialVersionUID = 1L;
			@Override
            public void valueChange(MValueChangeEvent<Donor> event) {
                editDonor(event.getValue());
            }
        });
       
        // Check box 
        options.addStyleName("horizontal");
        options.setMultiSelect(true);        
        options.setImmediate(true);
        options.addItem(0);
        options.setItemCaption(0,"People able to donate");
        options.addItem(1);
        options.setItemCaption(1,"People not able to donate");       
        options.select(0);
        options.select(1); 
        options.addBlurListener(new BlurListener() {				
			private static final long serialVersionUID = 1L;
			@Override
			public void blur(BlurEvent event) {
				options.select(0);
		        options.select(1);			
			}
		});
        options.addValueChangeListener(new ValueChangeListener() {        	
			private static final long serialVersionUID = 1L;
			@Override
        	public void valueChange(ValueChangeEvent event) {				
				String valueString = String.valueOf(event.getProperty().getValue());				
        		if (valueString.equals("[0]")) {    
        			listDonorsByStatus("Allow");        			
        		} else if (valueString.equals("[1]")) { 
        			listDonorsByStatus("Not allow");        			
        		} else if (valueString.equals("[]")) { 
        			listDonorsByStatus("Any");        			
        		} else {        			       			
        			listDonorsByStatus("");        			
        		}
        	}
        });

        // Configure filters
        filterByName.setInputPrompt("Search by name");
        filterByName.addBlurListener(new BlurListener() {				
			private static final long serialVersionUID = 1L;
			@Override
			public void blur(BlurEvent event) {
				filterByName.setValue("");				
			}
		});
        filterByName.addTextChangeListener(new FieldEvents.TextChangeListener() {            
			private static final long serialVersionUID = 1L;
			@Override
            public void textChange(FieldEvents.TextChangeEvent textChangeEvent) {				
                listDonors(textChangeEvent.getText());
            }
        });
        
        filterByBloodType.setInputPrompt("Search by blood type");
        filterByBloodType.addBlurListener(new BlurListener() {				
			private static final long serialVersionUID = 1L;
			@Override
			public void blur(BlurEvent event) {
				filterByBloodType.setValue("");				
			}
		});
        filterByBloodType.addTextChangeListener(new FieldEvents.TextChangeListener() {            
			private static final long serialVersionUID = 1L;
			@Override
            public void textChange(FieldEvents.TextChangeEvent textChangeEvent) {				
				String event = textChangeEvent.getText();				
				if (event.isEmpty()) {					
					listDonors();
				} else {					
					for (DonorBloodType bt : DonorBloodType.values()) {						
						if (bt.name().toLowerCase().startsWith(event.toLowerCase())) {							
							listDonorsByBloodType(bt);							
							break;
						} 
					}	
				}							
			}
        });
        
        filterByLocation.setInputPrompt("Search by location");
        filterByLocation.addBlurListener(new BlurListener() {				
			private static final long serialVersionUID = 1L;
			@Override
			public void blur(BlurEvent event) {
				filterByLocation.setValue("");				
			}
		});
        filterByLocation.addTextChangeListener(new FieldEvents.TextChangeListener() {           	
			private static final long serialVersionUID = 1L;
			@Override
            public void textChange(FieldEvents.TextChangeEvent textChangeEvent) {				
				String event = textChangeEvent.getText();
				if (event.isEmpty()) {					
					listDonors();
				} else {					
					for (DonorLocation loc : DonorLocation.values()) {						
						if (loc.name().toLowerCase().startsWith(event.toLowerCase())) {							
							listDonorsByLocation(loc);							
							break;
						} 
					}	
				}								
            }
        });

        // Responsive design configuring visible columns in table based on available width
        layout();
        adjustTableColumns();
        UI.getCurrent().setResizeLazy(true);
        Page.getCurrent().addBrowserWindowResizeListener(new Page.BrowserWindowResizeListener() {                    
        	private static final long serialVersionUID = 1L;
        	@Override
        	public void browserWindowResized(Page.BrowserWindowResizeEvent browserWindowResizeEvent) {
        		adjustTableColumns();
        		layout();
        	}
        });
        listDonors();		
	}

}
