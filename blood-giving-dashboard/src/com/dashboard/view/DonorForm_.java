/** Package */
package com.dashboard.view;

/** Imports */
import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.inject.Inject;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.fields.TypedSelect;
import org.vaadin.viritin.form.AbstractForm;
import org.vaadin.viritin.label.Header;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;
import com.dashboard.model.Donor;
import com.dashboard.model.DonorGender;
import com.dashboard.model.DonorService;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * A UI component built to modify Donor entities thanks to a form
 * 
 */
public class DonorForm_ extends AbstractForm<Donor> {

    /** Attributes */
	private static final long serialVersionUID = 1L;

	@Inject
    DonorService service;
    
    TextField firstName = new MTextField("First name").withFullWidth();
    TextField lastName = new MTextField("Last name").withFullWidth();
    DateField birthDate = new DateField("Birth day");    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	TypedSelect<String> status = new TypedSelect().withCaption("Status");
    OptionGroup gender = new OptionGroup("Gender");
    TextField email = new MTextField("Email").withFullWidth();
    
    /** Methods */

    @Override
    protected Component createContent() {
        setStyleName(ValoTheme.LAYOUT_CARD);
        return new MVerticalLayout(new Header("Donor form").setHeaderLevel(3),
        		new MFormLayout(firstName,lastName,email,birthDate,gender,status).withFullWidth()).withStyleName(ValoTheme.LAYOUT_CARD);
        		//new MFormLayout(firstName,lastName,email,birthDate,gender,status).withFullWidth(),getToolbar()).withStyleName(ValoTheme.LAYOUT_CARD);
    }

    @PostConstruct
    void init() {
    	
    	
        setEagerValidation(true);
        status.setWidthUndefined();
        status.setOptions(status.getValue());
        gender.addItems((Object[]) DonorGender.values());
        gender.setStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
        setSavedHandler(new SavedHandler<Donor>() {
            @Override
            public void onSave(Donor entity) {
                try {                    
                    service.saveEntity(entity);                    
                    saveEvent.fire(entity);
                } catch (EJBException e) {                    
                    Notification.show("Ups, something fails. Your changes were discarded.",Notification.Type.ERROR_MESSAGE);
                    refreshEvent.fire(entity);
                }
            }
        });
        
        setResetHandler(new ResetHandler<Donor>() {
            @Override
            public void onReset(Donor entity) {
            	refreshEvent.fire(entity);
            }
        });
        
        setDeleteHandler(new DeleteHandler<Donor>() {
            @Override
            public void onDelete(Donor entity) {
                service.deleteEntity(getEntity());
                deleteEvent.fire(getEntity());
            }
        });
    }

    @Override
    protected void adjustResetButtonState() {        
        getResetButton().setEnabled(true);
        getDeleteButton().setEnabled(getEntity() != null && getEntity().isPersisted());
    }
    
    @Inject
    @DonorEvent(DonorEvent.Type.SAVE)
    javax.enterprise.event.Event<Donor> saveEvent;

    @Inject
    @DonorEvent(DonorEvent.Type.REFRESH)
    javax.enterprise.event.Event<Donor> refreshEvent;

    @Inject
    @DonorEvent(DonorEvent.Type.DELETE)
    javax.enterprise.event.Event<Donor> deleteEvent;
    
}
