package com.dashboard.view;

import java.util.Date;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.vaadin.addon.leaflet.LMap;
import org.vaadin.addon.leaflet.LMarker;
import org.vaadin.addon.leaflet.LOpenStreetMapLayer;
import org.vaadin.addon.leaflet.LeafletClickEvent;
import org.vaadin.addon.leaflet.LeafletClickListener;
import org.vaadin.addon.leaflet.control.LZoom;
import org.vaadin.addon.leaflet.shared.ControlPosition;
import org.vaadin.cdiviewmenu.ViewMenuUI;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.label.Header;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import com.dashboard.model.BloodBank;
import com.dashboard.model.BloodBankService;
import com.dashboard.presenter.ScreenSize;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

@CDIView("bank")
public class BloodBankView extends DashboardView {

	/** Attributes */
	private static final long serialVersionUID = 1L;		
	
	@Inject
    private BloodBankService service;
	
	// GUI    
	@SuppressWarnings({ "unchecked", "rawtypes" })	
	LMap map = new LMap();	
	LZoom zoom = new LZoom();	
	Header header = new Header("Blood banks").setHeaderLevel(2);   
	OptionGroup options = new OptionGroup();
	DateField date = new DateField("Search by date");   
	Button addButton = new MButton(FontAwesome.EDIT,new Button.ClickListener() {              
		private static final long serialVersionUID = 1L;
		@Override
		public void buttonClick(Button.ClickEvent event) {
			addBloodBank();
		}
	});	

	/** Methods */
	@PostConstruct
	public void init() {		
		// Generate some dummy data
		service.resetRandomData();    	
		// Set up layout
		setDashboardLayout();		
		setContentArea();		
		setMenuArea();   		
		// Disable button
		manageMenuButton(Constants.BLOOD_BANK_VIEW,false);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

	}

	@Override
	void setContentArea() {
		// Add description
    	addButton.setDescription("Add new blood bank");
    	// Date picker resolution
    	date.setResolution(Resolution.DAY);  
    	date.setDateFormat(Constants.DATE_FORMAT);
    	date.setValue(new Date());
        // Check box 
        options.addStyleName("horizontal");
        options.setMultiSelect(true);        
        options.setImmediate(true);       
        options.addItem(0);
        options.setItemCaption(0,"Permanent centre");
        options.addItem(1);
        options.setItemCaption(1,"Mobile unit");       
        options.select(0);
        options.select(1); 
        options.addBlurListener(new BlurListener() {				
			private static final long serialVersionUID = 1L;
			@Override
			public void blur(BlurEvent event) {
				options.select(0);
		        options.select(1);			
			}
		});
        options.addValueChangeListener(new ValueChangeListener() {        	
			private static final long serialVersionUID = 1L;
			@Override
        	public void valueChange(ValueChangeEvent event) {
				String valueString = String.valueOf(event.getProperty().getValue());				
        		if (valueString.equals("[0]")) {    
        			setMap("Permanent");            		           		
        		} else if (valueString.equals("[1]")) { 
        			setMap("Mobile");
        		} else if (valueString.equals("[]")) { 
        			setMap("Any");
        		} else {        			       			
        			setMap();            		
        		}
        	}
        });        
        
        // Responsive layout
        layout();        
        UI.getCurrent().setResizeLazy(true);
        Page.getCurrent().addBrowserWindowResizeListener(new Page.BrowserWindowResizeListener() {                    
        	private static final long serialVersionUID = 1L;
        	@Override
        	public void browserWindowResized(Page.BrowserWindowResizeEvent browserWindowResizeEvent) {        		
        		layout();
        	}
        });
        setMap();		
	}
	
	private void layout() {
        removeAllComponents();
        if (ScreenSize.getScreenSize() == ScreenSize.LARGE) {
        	addComponents(new MHorizontalLayout(header).alignAll(Alignment.BOTTOM_LEFT));            
        	addComponents(new MHorizontalLayout(options,date,addButton).alignAll(Alignment.BOTTOM_LEFT));       	
        } else {
            addComponents(header,new MHorizontalLayout(options,date).alignAll(Alignment.BOTTOM_LEFT));
        }     
        expand(map);       
        zoom.setPosition(ControlPosition.topright);
        map.addControl(zoom);
        setMargin(new MarginInfo(false,true,false,true));
    }
	
	private void setMap() {		
		map.removeAllComponents();		
		LOpenStreetMapLayer osm = new LOpenStreetMapLayer();
		osm.setDetectRetina(true);		
		map.addComponent(osm);		
		for (final BloodBank b : service.findAll()) {		
			if(b.getLocation() != null) {		
				LMarker marker = new LMarker(b.getLocation());				
				marker.addClickListener(new LeafletClickListener() {
					@Override
					public void onClick(LeafletClickEvent event) {
						Notification.show("Mobility: " + b.getMobility() + " - Town: " + b.getTown() + " - City: " + b.getCity() + " - From: " + b.getTimetableFrom()+ " - To: " + b.getTimetableTo());								
					}
				});
				map.addComponent(marker);				
			}
		}
		map.zoomToContent();		
	}
	
	private void setMap(String str) {		
		map.removeAllComponents();		
		LOpenStreetMapLayer osm = new LOpenStreetMapLayer();
		osm.setDetectRetina(true);		
		map.addComponent(osm);		
		for (final BloodBank b : service.findByMobility(str)) {		
			if(b.getLocation() != null) {		
				LMarker marker = new LMarker(b.getLocation());				
				marker.addClickListener(new LeafletClickListener() {
					@Override
					public void onClick(LeafletClickEvent event) {
						Notification.show("Mobility: " + b.getMobility() + " - Town: " + b.getTown() + " - City: " + b.getCity() + " - From: " + b.getTimetableFrom()+ " - To: " + b.getTimetableTo());								
					}
				});
				map.addComponent(marker);				
			}
		}							
		map.zoomToContent();		
	}
	
	private void addBloodBank() {
		final Window window = new Window("Add new blood bank");
		window.setModal(true);
		window.setClosable(true);
		window.setResizable(false);
		window.setSizeUndefined();
		window.center();
		final BloodBankForm content = new BloodBankForm(service);
		content.init();
		window.setContent(content); 
		ViewMenuUI.getCurrent().addWindow(window);
	}

}