/** Package */
package com.dashboard.model;

/** Imports */
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * Donor entity
 *  
 */
@NamedQueries({
    @NamedQuery(name="Donor.findAll",query="SELECT c FROM Donor c"),
    @NamedQuery(name="Donor.findByName",query="SELECT c FROM Donor c WHERE LOWER(c.firstName) LIKE :filter OR LOWER(c.lastName) LIKE :filter"),
    @NamedQuery(name="Donor.findByBloodType",query="SELECT c FROM Donor c WHERE c.bloodType = :filter"),
    @NamedQuery(name="Donor.findByLocation",query="SELECT c FROM Donor c WHERE c.location = :filter"),
    @NamedQuery(name="Donor.findByStatus",query="SELECT c FROM Donor c WHERE c.status LIKE :filter")
})
@Entity
public class Donor implements Serializable {

    /** Attributes */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
    private int id;	
	
	@Column(name = "FIRSTNAME")
    private String firstName;
	
	@Column(name = "LASTNAME")
    private String lastName;

    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "BIRTHDATE")
    private Date birthDate;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "LASTDONATION")
    private Date lastDonation;
    
    @NotNull(message = "A valid email is required")
    @Pattern(regexp = ".+@.+\\.[a-z]+", message = "A valid email is required")
    @Column(name = "EMAIL")
    private String email;
        
    @Column(name = "GENDER")
    private DonorGender gender;
    
    @Column(name = "LOCATION")
    private DonorLocation location;
    
    @Column(name = "BLOODTYPE")
    private DonorBloodType bloodType;
    
    @Column(name = "RHFACTOR")
    private DonorRhFactor rhFactor;
    
    @Column(name = "STATUS")
    private String status;
    
    @Column(name = "CONTACTED")
    private String contacted;
    
    @Column(name = "TOTALDONATIONS")
    private int totalDonations;
    
    /** Constructor */
    public Donor() {
    	
    }
    
    public Donor(String firstName, String lastName, Date birthDate, Date lastDonation, DonorGender gender, DonorRhFactor rhFactor,
    		DonorLocation location, DonorBloodType bloodType, int totalDonations, String email, String status, String contacted) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.lastDonation = lastDonation;
		this.gender = gender;
		this.rhFactor = rhFactor;
		this.location = location;
		this.bloodType = bloodType;
		this.totalDonations = totalDonations;
		this.email = email;
		this.status = status;
		this.contacted = contacted;
	}

	/** Methods */   

    public boolean isPersisted() {
        return id > 0;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getLastDonation() {
		return lastDonation;
	}

	public void setLastDonation(Date lastDonation) {
		this.lastDonation = lastDonation;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DonorGender getGender() {
		return gender;
	}

	public void setGender(DonorGender gender) {
		this.gender = gender;
	}

	public DonorLocation getLocation() {
		return location;
	}

	public void setLocation(DonorLocation location) {
		this.location = location;
	}

	public DonorBloodType getBloodType() {
		return bloodType;
	}

	public void setBloodType(DonorBloodType bloodType) {
		this.bloodType = bloodType;
	}

	public int getTotalDonations() {
		return totalDonations;
	}

	public void setTotalDonations(int totalDonations) {
		this.totalDonations = totalDonations;
	}

	public String getDonorStatus() {
		return status;
	}

	public void setDonorStatus(String status) {
		this.status = status;
	}
	
	public String getContacted() {
		return contacted;
	}

	public void setContacted(String contacted) {
		this.contacted = contacted;
	}

	public DonorRhFactor getRhFactor() {
		return rhFactor;
	}

	public void setRhFactor(DonorRhFactor rhFactor) {
		this.rhFactor = rhFactor;
	}	
	
}