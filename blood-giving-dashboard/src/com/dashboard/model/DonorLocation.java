/** Package */
package com.dashboard.model;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * Donor location
 *  
 */
public enum DonorLocation {
	Castellon, Valencia , Alicante
}
