/** Package */
package com.dashboard.model;

/** Imports */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;

import com.vividsolutions.jts.geom.Point;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * BloodBank entity
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * AGPL-3.0
 *  
 */
@NamedQueries({   
    @NamedQuery(name="BloodBank.findByMobility",query="SELECT c FROM BloodBank c WHERE c.mobility LIKE :filter")
})
@Entity
public class BloodBank implements Serializable {

    /** Attributes */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
    private int id;	
	
	@Column(name = "MOBILITY")
    private String mobility;	

    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "AVAILABILITY")
    private Date availability;    
    
    @Column(name = "TIMETABLE-FROM")
    private String timetableFrom;
    
    @Column(name = "TIMETABLE-TO")
    private String timetableTo;
    
    @Column(name = "CITY")
    private String city;
    
    @Column(name = "TOWN")
    private String town;
    
    @Lob
    @Column(name = "LOCATION")
    private Point location;
    
    /** Constructor */
    public BloodBank() {
    	
    }
    
	public BloodBank(String mobility, Date availability, String timetableFrom, String timetableTo, String city, String town, Point location) {
		super();
		this.mobility = mobility;
		this.availability = availability;
		this.timetableFrom = timetableFrom;
		this.timetableTo = timetableTo;
		this.city = city;
		this.town = town;
		this.location = location;
	}
	
	/** Methods */ 

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMobility() {
		return mobility;
	}

	public void setMobility(String mobility) {
		this.mobility = mobility;
	}

	public Date getAvailability() {
		return availability;
	}

	public void setAvailability(Date availability) {
		this.availability = availability;
	}

	public String getTimetableFrom() {
		return timetableFrom;
	}

	public void setTimetableFrom(String timetableFrom) {
		this.timetableFrom = timetableFrom;
	}

	public String getTimetableTo() {
		return timetableTo;
	}

	public void setTimetableTo(String timetableTo) {
		this.timetableTo = timetableTo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}   
	
}