/** Package */
package com.dashboard.model;

/** Imports */
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.apache.commons.lang3.time.DateUtils;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * BloodBankService
 *  
 */
@Stateless
public class BloodBankService {
	
	/** Attributes */
    @PersistenceContext(unitName = "donor-pu")
    private EntityManager entityManager;    
    private static final String[] RANDOM_NAMES = new String[] {"Canals", "Paiporta", "Picanya", "Torrent", "Aldaia", "Benetusser", "Alfafar",
    	"Sedavi", "Albal", "Mislata", "Godelleta", "Cullera", "Gandia", "Xeraco", "Beniparrell", "Carlet", "San Antonio de Benager", 
    	"Paterna", "Rafelbunyol", "Algemesi", "Massanassa", "Alqueria de la Condesa", "Almussafes", "Benifaio", "Silla", "Moncada",
    	"Cheste", "Burjassot", "Naquera", "Riola", "Sueca", "Yatova", "Benicassim", "Altura", "Segorbe", "Chiva", "Cofrentes", "Sagunto",
    	"Puerto de Sagunto", "Manises"};
    private final double LAT_BASE = 39.42744680;
    private final double LON_BASE = -0.4184093;       
    
    /** Methods */
    public void saveEntity(BloodBank entity) {
        if (entity.getId() > 0) {
            entityManager.merge(entity);
        } else {
            entityManager.persist(entity);
        }
    }

    public void deleteEntity(BloodBank entity) {
        if (entity.getId() > 0) {            
            entity = entityManager.merge(entity);
            entityManager.remove(entity);
        }
    }

    public List<BloodBank> findAll() {    	
        CriteriaQuery<BloodBank> cq = entityManager.getCriteriaBuilder().createQuery(BloodBank.class);        
        cq.select(cq.from(BloodBank.class));        
        return entityManager.createQuery(cq).getResultList();        
    }

    public List<BloodBank> findByMobility(String filter) {
        if (filter == null || filter.isEmpty()) {
            return findAll();
        }        
        return entityManager.createNamedQuery("BloodBank.findByMobility",BloodBank.class).setParameter("filter",filter).getResultList();
    }
//    
//    public List<Donor> findByBloodType(DonorBloodType filter) {
//        if (filter == null) 
//            return findAll();                
//        return entityManager.createNamedQuery("Donor.findByBloodType",Donor.class).setParameter("filter",filter).getResultList();
//    }
//    
//    public List<Donor> findByLocation(DonorLocation filter) {
//        if (filter == null) 
//            return findAll();                
//        return entityManager.createNamedQuery("Donor.findByLocation",Donor.class).setParameter("filter",filter).getResultList();
//    }
    
    public void resetRandomData() {    	
    	//entityManager.createQuery("DELETE FROM BloodBank c").executeUpdate(); 
        if (!findAll().isEmpty()) {        
        	entityManager.createQuery("DELETE FROM BloodBank c").executeUpdate();         
        }       
        generateRandomData();    	    
    }

    public void generateRandomData() {		   
    	if (findAll().isEmpty()) {       
    		for (int i = 0; i < RANDOM_NAMES.length; i++) {
    			BloodBank b = new BloodBank();        
    			b.setMobility(getRandomMobility());
    			b.setAvailability(getRandomAvailabilty());
    			b.setTimetableFrom(getRandomTimetableFrom());
    			b.setTimetableTo(getRandomTimetableTo());
    			b.setCity(getRandomCity());
    			b.setTown(RANDOM_NAMES[i]);
    			b.setLocation(getRandomLocation());        		        		
    			saveEntity(b);       		
    		}
    	}
    }
   
   public String getRandomMobility() {
	   String[] mobility = {"Permanent", "Mobile"};
	   return mobility[(int) (Math.random() * mobility.length)];	
   }
   
   public String getRandomCity() {
	   String[] city = {"Castellon", "Valencia", "Alicante"};
	   return city[(int) (Math.random() * city.length)];	
   }
   
   public Point getRandomLocation() {	  
	   Random r = new Random();	   
	   GeometryFactory factory = new GeometryFactory();
	   double lat = LAT_BASE + 0.02 * r.nextDouble() - 0.02 * r.nextDouble();
       double lon = LON_BASE + 0.02 * r.nextDouble() - 0.02 * r.nextDouble();
       return factory.createPoint(new Coordinate(lon,lat));       
   }
   
   public Date getRandomAvailabilty() {
	   Random r = new Random();	   
	   return getRandomtWeekDate(r.nextInt(20));  
   }   
       
   public Date getRandomtWeekDate(int week) {
	   return  DateUtils.addWeeks(new Date(),week);
   }
   
   public String getRandomTimetableFrom() {
	   String[] from = {"08:00 am", "08:30 am", "09:00 am", "09:30 am", "10:00 am", "10:30 am", "11:00 am", "11:30 am", "12:00 pm", "12:30 pm"};
	   return from[(int) (Math.random() * from.length)];	
   }
   
   public String getRandomTimetableTo() {
	   String[] from = {"03:00 pm", "03:30 pm", "04:00 pm", "04:30 pm", "05:00 pm", "05:30 pm", "06:00 pm", "06:30 pm", "07:00 pm", "07:30 pm"};
	   return from[(int) (Math.random() * from.length)];	
   }

}