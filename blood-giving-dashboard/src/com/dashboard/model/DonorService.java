/** Package */
package com.dashboard.model;

/** Imports */
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import com.dashboard.view.Constants;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.apache.commons.lang3.time.DateUtils;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com) 
 * March, 2015
 * 
 * Donor service
 *  
 */
@Stateless
public class DonorService {
	
	/** Attributes */
    @PersistenceContext(unitName = "donor-pu")
    private EntityManager entityManager;    
    private static final String[] RANDOM_NAMES = new String[] {"Ada Lovelace", "Alan Turing", "Albert Einstein", "Alexander Fleming", "Alexander Graham-Bell",
    	"Anders Celsius", "Benjamin Franklin", "Blaise Pascal", "Carl Friedrich-Gauss", "Carl Sagan", "Charles Darwin", "Dmitri Mendeleev", "Galileo Galilei", 
    	"Guglielmo Marconi", "Isaac Newton", "James Clerk-Maxwell", "John Von-Neumann", "Konrad Lorenz", "Lee De-Forest", "Louis Pasteur", "Leonhard Euler",
    	"Leonardo da-Vinci", "Leon Foucault", "Stephen Hawking", "Santiago Ramon-Cajal", "Richard Feynman", "Pierre de-Fermat", "Nicolaus Copernicus", 
    	"Nikola Tesla"};
    
    /** Methods */
    public void saveEntity(Donor entity) {    	
        if (entity.getId() > 0) {
            entityManager.merge(entity);
        } else {
            entityManager.persist(entity);
        }
    }

    public void deleteEntity(Donor entity) {
        if (entity.getId() > 0) {            
            entity = entityManager.merge(entity);
            entityManager.remove(entity);
        }
    }

    public List<Donor> findAll() {  
    	List<Donor> output;
        CriteriaQuery<Donor> cq = entityManager.getCriteriaBuilder().createQuery(Donor.class);        
        cq.select(cq.from(Donor.class));
        output = entityManager.createQuery(cq).getResultList();        
        return output;
//        return entityManager.createQuery(cq).getResultList();        
    }

    public List<Donor> findByName(String filter) {
    	List<Donor> output;
        if (filter == null || filter.isEmpty()) {
            return findAll();
        }
        filter = filter.toLowerCase();
        output = entityManager.createNamedQuery("Donor.findByName",Donor.class).setParameter("filter",filter + "%").getResultList();
        Constants.notificationReceivers = output.size();
        return output;
        //return entityManager.createNamedQuery("Donor.findByName",Donor.class).setParameter("filter",filter + "%").getResultList();
    }
    
    public List<Donor> findByBloodType(DonorBloodType filter) {
    	List<Donor> output;
        if (filter == null) 
            return findAll(); 
        output = entityManager.createNamedQuery("Donor.findByBloodType",Donor.class).setParameter("filter",filter).getResultList();
        Constants.notificationReceivers = output.size();
        return output;
        //return entityManager.createNamedQuery("Donor.findByBloodType",Donor.class).setParameter("filter",filter).getResultList();
    }
    
    public List<Donor> findByLocation(DonorLocation filter) {
    	List<Donor> output;
        if (filter == null) 
            return findAll();
        output = entityManager.createNamedQuery("Donor.findByLocation",Donor.class).setParameter("filter",filter).getResultList();
        Constants.notificationReceivers = output.size();
        return output;
        //return entityManager.createNamedQuery("Donor.findByLocation",Donor.class).setParameter("filter",filter).getResultList();
    }
    
    public List<Donor> findByStatus(String filter) {
    	List<Donor> output;
        if (filter == null || filter.isEmpty()) {
            return findAll();
        } 
        output = entityManager.createNamedQuery("Donor.findByStatus",Donor.class).setParameter("filter",filter).getResultList();
        Constants.notificationReceivers = output.size();
        return output;
        //return entityManager.createNamedQuery("Donor.findByStatus",Donor.class).setParameter("filter",filter).getResultList();
    }
    
    public void resetRandomData() {    	
    //	entityManager.createQuery("DELETE FROM Donor c").executeUpdate(); 
        if (!findAll().isEmpty()) {        
        	entityManager.createQuery("DELETE FROM Donor c").executeUpdate();         
        }       
        generateRandomData();        
    }

   public void generateRandomData() {		   
        if (findAll().isEmpty()) {       
        	for (int i = 0; i < RANDOM_NAMES.length; i++) {
        		Donor c = new Donor();        
        		// Set firstName, lastName and email
        		String firstName = RANDOM_NAMES[i].split(" ")[0];
        		String lastName = RANDOM_NAMES[i].split(" ")[1];
        		c.setFirstName(firstName);
        		c.setLastName(lastName);
        		c.setEmail(firstName.toLowerCase() + "@" + lastName.toLowerCase() + ".com");       
        		// Set gender, location and blood type
        		c.setGender(getRandomGender(firstName));        		
        		c.setLocation(getRandomLocation());        		
        		c.setBloodType(getRandomBloodType());       
        		c.setRhFactor(getRandomRhFactor());        		
        		c.setTotalDonations(getRandomTotalDonations());        		
        		c.setBirthDate(getRandomBirthDate());       
        		c.setLastDonation(getRandomLastDonation()); 
        		String status = getRandomStatus();       
        		c.setDonorStatus(status);       
        		if (status.equals("Allow")) {       
        			c.setContacted(getRandomContacted());
        		} else {
        			c.setContacted("-");
        		}        		
        		saveEntity(c);       		
            }
        }
    }
   
   public DonorGender getRandomGender(String firstName) {	   
	   if(firstName.equals("Ada"))	   
		   return DonorGender.Female;
	   else
		   return DonorGender.Male;
   }
   
   public DonorLocation getRandomLocation() {   	   
	   return DonorLocation.values()[(int) (Math.random() * DonorLocation.values().length)];	     	   
   }
   
   public DonorBloodType getRandomBloodType() {
	   return DonorBloodType.values()[(int) (Math.random() * DonorBloodType.values().length)];	   
   } 
   
   public DonorRhFactor getRandomRhFactor() {
	   return DonorRhFactor.values()[(int) (Math.random() * DonorRhFactor.values().length)];	   
   }
   
   public int getRandomTotalDonations() {	   
	   Random r = new Random();
	   return r.nextInt(15);
   }
   
   public Date getRandomBirthDate() {
	   Random r = new Random();	   
	   return getRandomtYearDate(r.nextInt(75) + 1);
   }
   
   public Date getRandomLastDonation() {
	   Random r = new Random();	   
       return getRandomtWeekDate(r.nextInt(20) + 1);       
   }
   
   public String getRandomStatus() {
	   String[] status = {"Allow", "Not allow"};
	   return status[(int) (Math.random() * status.length)];	
   }
   
   public String getRandomContacted() {
	   String[] contacted = {"Yes", "No"};
	   return contacted[(int) (Math.random() * contacted.length)];	
   }
   
   public String getTimestamp() {
	   Date date = new Date();	   
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	   try {
		   sdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));
	   } catch (Exception ex) {

	   }
	   return sdf.format(date);
   }
   
   public Date getDateFromTimestamp(String timeStamp){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));
		try {
			return sdf.parse(timeStamp);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
   
   public Date getYesterdayDate() {
	   return  DateUtils.addDays(new Date(),-1);
   }
   
   public Date getRandomDayDate(int day) {
	   return  DateUtils.addDays(new Date(),-day);
   }

   public Date getLastWeekDate() {
	   return  DateUtils.addWeeks(new Date(),-1);
   }
   
   public Date getRandomtWeekDate(int week) {
	   return  DateUtils.addWeeks(new Date(),-week);
   }

   public Date getLastMonthDate() {
	   return DateUtils.addMonths(new Date(),-1);
   }
   
   public Date getRandomtMonthDate(int month) {
	   return DateUtils.addMonths(new Date(),-month);
   }
   
   public Date getRandomtYearDate(int year) {	   
	  return DateUtils.addYears(new Date(),-year);
   }

}