/** Package */
package com.dashboard.presenter;

/** Imports */
import javax.inject.Inject;
import org.vaadin.cdiviewmenu.ViewMenuUI;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.Widgetset;
import com.vaadin.cdi.CDIUI;
import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

/** 
 * @author Angel Martinez-Cavero (anmarca6@gmail.com)
 * March, 2015
 * 
 * Main class of the whole application. Starting point. We are using Vaadin CDI for setting up the navigator and views, Furthermore,
 * we are using the helper class ViewMenuUI (org.vaadin.cdiviewmenu) which provides us a basic main layout with ViewMenu and ViewMenuLayout,
 * and configures the Navigator automatically
 *  
 */
@CDIUI("")
@Title("Give blood, save lives")
@Theme("blood-giving-dashboard")
@Widgetset("com.dashboard.presenter.widgetset.Blood_giving_dashboardWidgetset")
public class MainUI extends ViewMenuUI {
	
	/** Attributes */
	private static final long serialVersionUID = 1L;
	private Navigator navigator;

	@Inject
	CDIViewProvider viewProvider;

	/** Methods */

	/** Return the currently active UI instance with correct type */
    public static MainUI get() {
        return (MainUI) UI.getCurrent();
    }

	@Override
	protected void init(VaadinRequest request) {
		navigator = new Navigator(this,this);
        navigator.addProvider(viewProvider);	
        navigator.navigateTo("");
	}

}